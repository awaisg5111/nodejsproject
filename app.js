require('./models/db');

const express = require('express');
const bodyparser = require('body-parser');
const expressLayouts = require('express-ejs-layouts');
const mongoose = require('mongoose');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require('passport');

const app = express();

// Passport Config
require('./config/passport')(passport);

//EJS
app.set('view engine', 'ejs');

//dir path
app.use(express.static("assets"));

//Body Parser
app.use(bodyparser.urlencoded({ extended: false }));

// Express session
app.use(
    session({
      secret: 'secret',
      resave: true,
      saveUninitialized: true
    })
  );

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

// Connect flash
app.use(flash());

//Global var
app.use((req, res, next) => {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.login = req.isAuthenticated();
    next();
});

//Routes
app.use('/',require('./routes/index'));
app.use('/admin',require('./routes/admin'));

const PORT = process.env.PORT || 5000;

app.listen(PORT,console.log(`Server is started at port ${PORT}`));