const express = require('express');
const passport = require('passport');

const router = express.Router();

const User = require('../models/user.model');
const Product = require('../models/product.model');

const { ensureAdminAuthenticated, forwardAdminAuthenticated } = require('../config/auth');

//Main page
router.get('/', ensureAdminAuthenticated, (req, res) =>
  res.render('admin/dashboard', {
    user: req.user
  })
);

//Login
router.get('/login',(req , res, next) => {
    if(req.isAuthenticated()){
        res.redirect('admin');
    }
    else {
        res.render('admin/login');
    }
  });

router.post('/login', (req, res, next) => {
  passport.authenticate('local', {
    successRedirect: '/admin',
    failureRedirect: '/admin/login',
    failureFlash: true
  })(req, res, next);
});

// Logout
router.get('/logout', (req, res) => {
  req.logout();
  req.flash('success_msg', 'You are logged out');
  res.redirect('/admin/login');
});

//User Route
router.get('/users', ensureAdminAuthenticated, (req, res) => {
  User.find((err, docs) => {
      if (!err) {
          res.render("admin/users/index", {
              list: docs
          });
      }
      else {
          console.log('Error in retrieving users list :' + err);
      }
  });
});

//Product Route
router.get('/products', (req, res) => {
  Product.find((err, docs) => {
      if (!err) {
          res.render("admin/products/index", {
              list: docs
          });
      }
      else {
          console.log('Error in retrieving users list :' + err);
      }
  });
});

router.get('/addproduct', (req , res) => {
  res.render('admin/products/addorEdit');
});


module.exports = router;