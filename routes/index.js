const express = require('express');
const bcrypt = require('bcryptjs');
const passport = require('passport');

const router = express.Router();
const User = require('../models/user.model');

const { ensureAuthenticated, forwardAuthenticated } = require('../config/auth');

//Main page
router.get('/',(req , res) => res.render('index'));

//Login
router.get('/login',(req , res, next) => {
  if(req.isAuthenticated()){
      res.redirect('/');
  }
  else {
      res.render('login');
  }
});

router.post('/login', (req, res, next) => {
  passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true
  })(req, res, next);
});

// Logout
router.get('/logout', (req, res) => {
  req.logout();
  req.flash('success_msg', 'You are logged out');
  res.redirect('/login');
});

//Register
router.get('/register',(req , res, next) => {
  if(req.isAuthenticated()){
      res.redirect('/');
  }
  else {
      res.render('register');
  }
});

//Register Handle
router.post('/register',(req , res) => {
  const { name, email, password, password2 } = req.body;
  let errors = [];
  // Mongoose Model.findOne()
  User.findOne({email:email}).then(user=>{
    if(user){
        errors.push({msg: 'Email already exists'});
        res.render('register',{errors})
    }
  });
  //check required fields
  if(!name || !email || !password || !password2) {
      errors.push({ msg: 'Please fill in all fields' });
  }

  //check password match
  if(password !== password2) {
      errors.push({ msg: 'Password do not match' });
  }

  //Check password length
  if(password.length < 6) {
      errors.push({ msg: 'Password should be at least 6 characters' });
  }
  if(errors.length > 0) {
      res.render('register', {
          errors,
          name,
          email,
          password,
          password2
      });
  }
  else {
      const newUser = new User({
          name,
          email,
          password
      });
      
      //Hash Password
      bcrypt.genSalt(10, (err,salt) => {
          bcrypt.hash(newUser.password, salt, (err, hash) => {
              if(err) throw err;

              newUser.password = hash;
              newUser.save()
                  .then(user => {
                      req.flash(
                          'success_msg',
                          'You are now registered and can log in'
                      );
                      res.redirect('/login')
                  })
                  .catch(err => console.log(err));
          })
      });
  }
});

module.exports = router;
