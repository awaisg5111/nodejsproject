module.exports = {
    ensureAuthenticated: function(req, res, next) {
      if (req.isAuthenticated()) {
        return next();
      }
      req.flash('error_msg', 'Please log in to view that resource');
      res.redirect('/login');
    },
    ensureAdminAuthenticated: function(req, res, next) {
      if (req.isAuthenticated() && req.user.role == 1 ) {
        return next();
      }
      req.flash('error_msg', 'Please log in to view that resource');
      res.redirect('/admin/login');
    },
    forwardAuthenticated: function(req, res, next) {
      if (!req.isAuthenticated()) {
        return next();
      }
      res.redirect('/');      
    },
    forwardAdminAuthenticated: function(req, res, next) {
      if (!req.isAuthenticated()) {
        return next();
      }
      res.redirect('/admin');      
    }
  };